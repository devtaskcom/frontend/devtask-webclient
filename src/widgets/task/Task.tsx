import Divider from "../../shared/Divider/Divider"
import MainButton from "../../shared/MainButton/MainButton"
import StatusItem from "../../shared/StatusItem/StatusItem"
import Time from "../../shared/Time/Time"
import mod from "./Task.module.scss"

const Task = () => {
  return (
    <>
      <div className={mod.card}>
        <div className={mod.header}>
          <Time />
          <StatusItem status="" />
        </div>
        <div className={mod.text}>Develop a component for a slider with the ability to display a different number of slides</div>
        <Divider />
        <div className={mod.buttons}>
          <MainButton text="Mark as Completed" theme="white" paddingY={16} fullWidth radius={5} />
          <MainButton text="Completed" theme="black" paddingY={16} fullWidth radius={5} />
          <MainButton text="Mark as Failed" theme="gray" paddingY={16} fullWidth radius={5} />
        </div>
      </div>
      <div className={mod.card}>
        <div className={mod.header}>
          <Time />
          <StatusItem status="completed" />
        </div>
        <div className={mod.text}>Develop a component for a slider with the ability to display a different number of slides</div>
        <Divider />
        <div className={mod.buttons}>
          <MainButton text="Mark as Completed" theme="white" paddingY={16} fullWidth radius={5} />
          <MainButton text="Completed" theme="black" paddingY={16} fullWidth radius={5} />
          <MainButton text="Mark as Failed" theme="gray" paddingY={16} fullWidth radius={5} />
        </div>
      </div>
      <div className={mod.card}>
        <div className={mod.header}>
          <Time />
          <StatusItem status="failed" />
        </div>
        <div className={mod.text}>Develop a component for a slider with the ability to display a different number of slides</div>
        <Divider />
        <div className={mod.buttons}>
          <MainButton text="Mark as Completed" theme="white" paddingY={16} fullWidth radius={5} />
          <MainButton text="Completed" theme="black" paddingY={16} fullWidth radius={5} />
          <MainButton text="Mark as Failed" theme="gray" paddingY={16} fullWidth radius={5} />
        </div>
      </div>
    </>

  )
}

export default Task