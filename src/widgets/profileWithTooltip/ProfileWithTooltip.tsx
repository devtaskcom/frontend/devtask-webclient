import { useState } from "react"
import cl from 'classnames'
import mod from './ProfileWithTooltip.module.scss'
import Tooltip from "../../shared/Tooltip/Tooltip"
import { Icon } from "../../utils/icon/icon"
import SpanWithTheme from "../../shared/SpanWithTheme/SpanWithTheme"
import { SpanWithThemeEnum } from "../../shared/SpanWithTheme/types"
import UserAvatar, { SizeIconEnum } from "../../shared/UserAvatar/UserAvatar"
import { Link } from "react-router-dom"

const ProfileWithTooltip = () => {

  const [showToolTip, setShowToolTip] = useState(false);

  const onMouseEnterHandler = () => {
    setShowToolTip(true);
  };

  const onMouseLeaveHandler = () => {
    setShowToolTip(false);
  };

  return (
    <div className={cl(mod.container)} onMouseEnter={onMouseEnterHandler} >
      <UserAvatar sizeIcon={SizeIconEnum['SM']} />
      {showToolTip && (<div className={mod.tooltip} onMouseLeave={onMouseLeaveHandler}>
        <Tooltip>
          <div className={mod.col}>
            <Link className={mod.link} to='/profile'>
              <Icon name="profile" />
              <SpanWithTheme theme={SpanWithThemeEnum["Light-3"]} >Profile</SpanWithTheme>
            </Link>
            <Link className={mod.link} to='/logOut'>
              <Icon name="logOut" />
              <SpanWithTheme theme={SpanWithThemeEnum["Accent-3"]} >Exit</SpanWithTheme>
            </Link>
          </div>
        </Tooltip >
      </div>)}
    </div>
  )
}
export default ProfileWithTooltip
