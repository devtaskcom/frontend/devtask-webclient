import { useState, useCallback } from 'react';
import TextInput from '../../shared/TextInput/TextInput';
import Typogrphy from '../../shared/Typography/Typography';
import MainButton from '../../shared/MainButton/MainButton';
import { Link, useNavigate } from 'react-router-dom';
import { useLoginMutation } from '../../services/auth/Auth.api';
import { LoginDataReq } from '../../types/authDTO/authDTO';
import mod from './SignInForm.module.scss';
import { useDispatch } from 'react-redux';
import { setUserId } from '../../redux/userIdSlice';

const initialData = {
  email: '',
  password: '',
};

const SignInForm = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [login] = useLoginMutation();
  const [loginData, setLoginData] = useState<LoginDataReq>(initialData);
  const [userId, setAuthUserId] = useState('');

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const result = await login(loginData);
    const id = 'data' in result ? result.data.id : '';
    setAuthUserId(id);
    setLoginData(initialData);
    if (userId && userId === id) {
      dispatch(setUserId(userId));
      navigate('/')
    }
  };

  const handleChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>): void =>
      setLoginData((p) => ({ ...p, [e.target.name]: e.target.value })),
    [],
  );

  const githubAuth = () => {
    window.open('http://apicalc.click/api/v1/auth/github', '_self');
  };

  const googleAuth = () => {
    window.open('http://apicalc.click/api/v1/auth/google', '_self');
  };

  return (
    <>
      <div className={mod.text}>
        <Typogrphy text="log in to your account" value="h2" />
      </div>

      <form onSubmit={(e) => handleSubmit(e)} className={mod.card}>
        <TextInput
          placeholder="Email or Phone"
          name="email"
          type="email"
          onChange={(e) => handleChange(e)}
          required
          min="4"
          value={loginData.email}
        />
        <TextInput
          placeholder="Password"
          type="password"
          name="password"
          onChange={(e) => handleChange(e)}
          required
          min="6"
          value={loginData.password}
        />
        <MainButton text="Log In" paddingX={16} flex="center" radius={10} />
        <MainButton
          onClick={githubAuth}
          text="GitHub"
          theme="white"
          flex="center"
          paddingX={16}
          radius={10}
        />
        <MainButton
          onClick={googleAuth}
          text="Google"
          theme="white"
          flex="center"
          paddingX={16}
          radius={10}
        />
        <Link to="/" className={mod.remind}>
          Forgot your password?
        </Link>
      </form>
    </>
  );
};

export default SignInForm;
