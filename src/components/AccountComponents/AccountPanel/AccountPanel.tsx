import { AccountPanelProps } from '../type';
import SubButton from '../../../shared/SubButton/SubButton';

import mod from './AccountPanel.module.scss';
import { Icon } from '../../../utils/icon/icon';

const AccountPanel: React.FC<AccountPanelProps> = ({
  urlImage,
  completed,
  failed,
  setIsFormOpen,
}) => {
  return (
    <div className={mod.panel}>
      <div className={mod.statistics}>
        <div className={mod.box}>
          <span className={mod.circleGreen} />
          <span className={mod.completed}>{completed}</span>
        </div>
        <div className={mod.box}>
          <span className={mod.circleRed}></span>
          <span className={mod.failed}>{failed}</span>
        </div>
      </div>
      <div className={mod.user}>
        <div className={mod.image}>
          <img src={urlImage} alt="foto" />
        </div>
        <button className={mod.plus}>
          <Icon name="plusCircleIcon" />
        </button>
      </div>
      <SubButton
        link={false}
        text="Change Password"
        onClick={() => setIsFormOpen(true)}
      />
    </div>
  );
};

export default AccountPanel;
