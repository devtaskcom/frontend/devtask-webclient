export type UserData = {
  id: number;
  personalData: {
    lastName: string;
    firstName: string;
    dateOfBirth: string;
    nickName: string;
    gender: number;
    urlImage: string;
    email: string;
    phone: string;
  };
  statistics: {
    completed: number;
    failed: number;
  };
};

export type AccountProps = {
  id: number;
};

export type AccountPanelProps = {
  urlImage: string;
  completed: number;
  failed: number;
  setIsFormOpen: (value: boolean) => void;
};

export type AccountHeaderProps = {
  setIsConfirmOpen: (value: boolean) => void;
};
