import SubButton from '../../../shared/SubButton/SubButton';
import { AccountHeaderProps } from '../type';
import mod from './AccountHeader.module.scss';

const AccountHeader: React.FC<AccountHeaderProps> = ({ setIsConfirmOpen }) => {
  return (
    <nav className={mod.nav}>
      <SubButton
        icon="backIcon"
        link={false}
        text="Back"
        onClick={() => setIsConfirmOpen(true)}
      />
      <SubButton
        icon="exitIcon"
        link={false}
        text="Exit"
        color="orange"
        onClick={() => setIsConfirmOpen(true)}
      />
    </nav>
  );
};

export default AccountHeader;
