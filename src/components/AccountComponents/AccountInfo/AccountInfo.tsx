import React from 'react';
import TextInput from '../../../shared/TextInput/TextInput.tsx';
import { UserData } from '../type.ts';

import mod from './AccountInfo.module.scss';

const FormSection: React.FC<{ userData: UserData }> = ({ userData }) => {
  const { personalData } = userData;
  const { lastName, firstName, nickName, dateOfBirth, email, phone } =
    personalData;
  const formInputs = [
    {
      label: 'Last Name:',
      value: lastName,
      name: 'lastName',
      type: 'text',
    },
    {
      label: 'First Name:',
      value: firstName,
      name: 'firstName',
      type: 'text',
    },
    {
      label: 'Nickname:',
      value: nickName,
      name: 'nickName',
      type: 'text',
    },
    {
      label: 'Date of birth:',
      value: dateOfBirth,
      name: 'dateOfBirth',
      type: 'text',
    },
    {
      label: 'E-mail:',
      value: email,
      name: 'email',
      type: 'email',
      disabled: true,
    },
    {
      label: 'Phone:',
      value: phone,
      name: 'phone',
      type: 'phone',
      disabled: true,
    },
  ];

  return (
    <form className={mod.info}>
      {formInputs.map((input) => (
        <TextInput
          key={input.name}
          classNames={mod.field}
          label={input.label}
          name={input.name}
          type={input.type}
          disabled={input.disabled}
          value={input.value}
          readOnly
        />
      ))}
    </form>
  );
};

export default FormSection;
