import { useState, useEffect } from 'react';
import { UserData } from './type.ts';

export const useFetchUserData = (id: number): UserData | null => {
  const [userData, setUserData] = useState<UserData | null>(null);

  useEffect(() => {
    fetch('https://64b3bb3b0efb99d8626851bb.mockapi.io/users')
      .then((response) => response.json())
      .then((data: UserData[]) => {
        const userId = data.find((user: UserData) => user.id === id);
        if (userId) {
          setUserData(userId);
        }
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  }, [id]);

  return userData;
};
