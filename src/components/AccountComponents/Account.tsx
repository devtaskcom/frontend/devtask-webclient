import { useState } from 'react';
import { AccountProps, UserData } from './type.ts';
import { useFetchUserData } from './useFetchUserData.ts';
import AccountPanel from './AccountPanel/AccountPanel.tsx';
import AccountInfo from './AccountInfo/AccountInfo.tsx';
import AccountHeader from './AccountHeader/AccountHeader.tsx';
import ModalForm from '../../shared/ModalForm/ModalForm.tsx';
import ModalConfirm from '../../shared/ModalConfirm/ModalConfirm.tsx';

const Account: React.FC<AccountProps> = ({ id }) => {
  const userData: UserData | null = useFetchUserData(id);
  const [isConfirmOpen, setIsConfirmOpen] = useState<boolean>(false);
  const [isFromOpen, setIsFromOpen] = useState<boolean>(false);

  if (!userData) {
    return <div>Loading...</div>;
  }
  return (
    <div>
      <AccountHeader setIsConfirmOpen={setIsConfirmOpen} />
      <AccountPanel
        setIsFormOpen={setIsFromOpen}
        urlImage={userData.personalData.urlImage}
        completed={userData.statistics.completed}
        failed={userData.statistics.failed}
      />
      <AccountInfo userData={userData} />
      <ModalConfirm
        isOpen={isConfirmOpen}
        setIsOpen={setIsConfirmOpen}
        text="Do you want to get out?"
      />
      <ModalForm isOpen={isFromOpen} setIsOpen={setIsFromOpen} />
    </div>
  );
};

export default Account;
