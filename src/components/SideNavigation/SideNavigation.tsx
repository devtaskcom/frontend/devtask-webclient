import { Link } from 'react-router-dom';
import { Icon } from '../../utils/icon/icon';
import mod from './SideNavigation.module.scss';

const SideNavigation = () => {
  return (
    <div>
      <div className={mod.header}>
        <Link to="/">
          <Icon name="logo" />
        </Link>
      </div>
      <div>
        <ul>
          <li>
            <Link to='/'>
              Home
            </Link>
          </li>
          <li>
            <Link to='/tasks'>
              Tasks
            </Link>
          </li>
          <li>
            <Link to='/projects'>
              Projects
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default SideNavigation;
