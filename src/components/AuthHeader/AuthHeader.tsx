import { Link, useLocation } from 'react-router-dom';
import { Icon } from '../../utils/icon/icon';
import SubButton from '../../shared/SubButton/SubButton';
import mod from './AuthHeader.module.scss';

const AuthHeader = () => {
  const location = useLocation();
  console.log(location);
  return (
    <div className={mod.header}>
      <Link to="/">
        <Icon name="logo" />
      </Link>
      <div className={mod.links}>
        <SubButton link={true} text={'Demo'} color={'orange'} to={'demo'} />
        {location.pathname === '/auth' ? (
          <SubButton link={true} text={'Sing Up'} to={'register'} />
        ) : (
          <SubButton link={true} text={'Sing In'} to={'/auth'} />
        )}
      </div>
    </div>
  );
};

export default AuthHeader;
