import wlp from '../../assets/icons/WLP.png'
import mod from './AuthFooter.module.scss'

const AuthFooter = () => {
  return (
    <div className={mod.footer}>
      <span>© we love pet</span>
      <img src={wlp} alt="WeLovePet" />
    </div>
  );
}

export default AuthFooter