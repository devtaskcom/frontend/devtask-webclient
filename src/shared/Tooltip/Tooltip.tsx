import { FC } from "react";
import mod from './Tooltip.module.scss'
import cl from 'classnames';
import { TooltipProps } from "./types";

const Tooltip: FC<TooltipProps> = ({ children, className, ...props }) => {
  return (
    <div {...props} className={cl(mod.container, className)}>{children}</div>
  )
}
export default Tooltip
