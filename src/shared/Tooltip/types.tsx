export type TooltipProps = {
  className?: string
  children: React.ReactNode
  [key: string]: any
}