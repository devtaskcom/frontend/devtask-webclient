// Import the correct React type and fix the import statement for the module
import { FC, ReactNode } from 'react';
import mod from './AuthCard.module.scss';

const AuthCard: FC<{ children: ReactNode }> = ({ children }) => {
  return (
    <div className={mod.card}>
      {children}
    </div>
  );
};

export default AuthCard;
