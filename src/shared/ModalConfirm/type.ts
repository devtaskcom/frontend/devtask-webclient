export interface ModalConfirmProps {
  isOpen: boolean;
  setIsOpen: (value: boolean) => void;
  text: string;
}
