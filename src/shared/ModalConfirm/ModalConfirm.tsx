import Typogrphy from '../Typography/Typography';
import MainButton from '../MainButton/MainButton';
import mod from './ModalConfirm.module.scss';
import { ModalConfirmProps } from './type';

const ModalConfirm: React.FC<ModalConfirmProps> = ({
  isOpen,
  setIsOpen,
  text,
}) => {
  if (!isOpen) return null;

  return (
    <div className={mod.overlay}>
      <div className={mod.modal}>
        <Typogrphy value="h2" text={text} mb={32} />
        <div className={mod.buttons}>
          <MainButton
            onClick={() => setIsOpen(false)}
            theme="white"
            text={'Yes'}
            fullWidth={true}
            flex={'center'}
            paddingX={8}
            paddingY={50}
            radius={10}
          />
          <MainButton
            onClick={() => setIsOpen(false)}
            theme="gray"
            text={'No'}
            fullWidth={true}
            flex={'center'}
            paddingX={8}
            paddingY={50}
            radius={10}
          />
        </div>
      </div>
    </div>
  );
};

export default ModalConfirm;
