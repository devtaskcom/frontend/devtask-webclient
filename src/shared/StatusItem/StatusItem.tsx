import { FC } from 'react';
import cl from "classnames";
import { StatusItemProps } from './type';
import mod from './StatusItem.module.scss';

const StatusItem: FC<StatusItemProps> = ({ status }) => <div className={

  cl(
  mod.marker,
  {
    [mod.completed]: status === 'completed',
    [mod.failed]: status === 'failed',
  }
)
}></div >

export default StatusItem;
