type TypogrphyProps = {
  value: 'h2' | 'h4' | 'button';
  text: string;
  color?: string;
  fullWidth?: boolean;
  centered?: boolean;
  mb?: 24 | 32 | 40 | 48;
};

const Typogrphy: React.FC<TypogrphyProps> = ({ value, text, color, centered, mb }) => {
  return (
    <div style={{
      display: 'inline-block',
      width: centered ? '100%' : '',
      textAlign: centered ? 'center' : 'start',
    }}>
      {(() => {
        switch (value) {
          case 'h2':
            return (
              <h2
                style={{
                  color: color ? color : '#25262C',
                  fontSize: 24,
                  fontWeight: 500,
                  marginBottom: mb,
                }}>
                {text}
              </h2>
            );
          case 'button':
            return (
              <span
                style={{
                  color: color ? color : '#25262C',
                  fontSize: 18,
                  fontWeight: 500,
                  paddingTop: 2,
                  marginBottom: mb,
                }}>
                {text}
              </span>
            );
          case 'h4':
            return (
              <h4
                style={{
                  color: color ? color : '#25262C',
                  fontSize: 13,
                  fontWeight: 700,
                  marginBottom: mb,
                }}>
                {text}
              </h4>
            );
        }
      })()}
    </div>
  );
};

export default Typogrphy;
