import { FC } from "react";
import { UnionProps } from "./types";
import mod from "./SpanWithTheme.module.scss"
import cl from 'classnames';

const SpanWithTheme: FC<UnionProps> = ({ classNames, theme, children, ...props }) => {
  const themedClass = theme ? mod[theme] : '';

  return (
    <span {...props} className={cl(mod.link, themedClass, classNames)}>
      {children}
    </span>
  )
}

export default SpanWithTheme
