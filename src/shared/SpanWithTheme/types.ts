
export enum SpanWithThemeEnum {
  'Dark' = 'dark',
  'Medium' = 'medium',
  'White' = 'white',
  'Light-3' = 'light-3',
  'Accent-3' = 'accent-3',
}

type SpanWithThemeProps = {
  theme?:SpanWithThemeEnum;
  children: React.ReactNode;
}
type OtherProps = {
  [key: string]: any;
}

export type UnionProps = SpanWithThemeProps & OtherProps;
