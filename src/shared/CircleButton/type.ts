export type CircleButtonProps = {
  icon?: any;
  backColor?: 'black' | 'white' | 'gray';
  paddingY?: number;
  paddingX?: number;
  border?: boolean;
};