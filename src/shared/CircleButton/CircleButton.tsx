import { CircleButtonProps } from './type';
import mod from './CircleButton.module.scss';

const CircleButton: React.FC<CircleButtonProps> = ({
  icon,
  backColor,
  paddingY,
  paddingX,
  border,
}) => {
  return (
    <button
      className={mod.button}
      style={{
        background:
          backColor === 'black'
            ? '#25262c'
            : backColor === 'white'
            ? '#ffffff'
            : backColor === 'gray'
            ? '#f4f3f7'
            : '#ffffff',
        paddingLeft: paddingY ? `${paddingY}px` : '6px',
        paddingRight: paddingY ? `${paddingY}px` : '6px',
        paddingTop: paddingX ? `${paddingX}px` : '6px',
        paddingBottom: paddingX ? `${paddingX}px` : '6px',
        border: border ? '1px solid #25262c' : '',
      }}
    >
      {icon ? <span>{icon}</span> : ''}
    </button>
  );
};

export default CircleButton;
