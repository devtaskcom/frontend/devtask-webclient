import { FC } from 'react'
import classNames from 'classnames';
import mod from './UserAvatar.module.scss'
import { icons } from '../../utils/icons'

export enum SizeIconEnum {
  'SM' = 'sm',
  'MD' = 'md',
}

type avatarProps = {
  path?: string;
  sizeIcon: SizeIconEnum
}

const UserAvatar: FC<avatarProps> = ({ path, sizeIcon }) => {
  return <div className={classNames(mod.avatar, mod[sizeIcon])} data-testid="avatar" role="img" >
    <img src={path || icons.avatar} alt="avatar" />
  </div>
}

export default UserAvatar