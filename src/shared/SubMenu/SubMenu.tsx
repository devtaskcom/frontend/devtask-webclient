import { SubMenuProps } from './type';
import mod from './SubMenu.module.scss';

const SubMenu: React.FC<SubMenuProps> = ({ children }) => {
  return <div className={mod.menu}>{children}</div>;
};

export default SubMenu;
