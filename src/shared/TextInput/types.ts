import { ChangeEvent } from "react";

export interface TextInputProps {
  label?: string;
  name: string;
  type?: string;
  placeholder?: string;
  error?: boolean | string;
  success?: boolean | string;
  mb?: 16 | 24; 
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  classNames?: string | undefined;
  [key: string]: unknown;
}
