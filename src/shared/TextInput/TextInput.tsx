import { FC, useState, memo, useCallback } from 'react';
import cl from 'classnames';
import { TextInputProps } from './types';
import { Icon } from '../../utils/icon/icon';
import mod from './TextInput.module.scss';

const TextInput: FC<TextInputProps> = ({
  label,
  name,
  type,
  placeholder,
  onChange,
  error,
  success,
  classNames,
  mb,
  ...props
}) => {
  const [eyeState, setEyeState] = useState(false);

  const handleToggleEye = useCallback(
    () => setEyeState((prev) => !prev),
    [eyeState]
  );
  const isPasswordType = type === 'password';

  const handleVisibleImg = (isPassword: boolean) => {
    if (isPassword) {
      return (
        <Icon
          name={eyeState ? 'openEye' : 'closeEye'}
          className={mod.img}
          onClick={handleToggleEye}
        />
      );
    } else {
      return null;
    }
  };

  return (
    <>
      <label className={cl(mod.label, classNames)}>
        {label || ''}
        <div className={cl(mod.wrapper)}>
          <input
            {...props}
            className={cl(mod.inputDefault, {
              [mod.inputError]: error,
              [mod.inputSuccess]: success,
              [mod.inputPassword]: isPasswordType,
            })}
            name={name}
            type={isPasswordType && eyeState ? 'text' : type}
            placeholder={placeholder}
            onChange={onChange}
          />
          {handleVisibleImg(isPasswordType)}
        </div>
      </label>
    </>
  );
};

export default memo(TextInput);
