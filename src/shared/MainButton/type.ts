import { MouseEvent } from "react";

export type MainButtonProps = {
  icon?: any;
  text?: string;
  theme?: 'black' | 'white' | 'gray';
  paddingY?: number;
  paddingX?: number;
  flex?: 'start' | 'center';
  fullWidth?: boolean;
  radius?: 5 | 10;
  onClick?: (e?: MouseEvent<HTMLButtonElement>) => void;
};
