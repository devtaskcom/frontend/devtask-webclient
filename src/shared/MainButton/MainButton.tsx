import { useState } from 'react';
import { MainButtonProps } from './type';
import Typogrphy from '../Typography/Typography';
import mod from './MainButton.module.scss';

const MainButton: React.FC<MainButtonProps> = ({
  icon,
  text,
  theme,
  paddingY,
  paddingX,
  flex,
  fullWidth,
  radius,
  onClick
}) => {
  const [isHover, setIsHover] = useState(false);

  const handleMouseEnter = () => {
    setIsHover(true);
  };
  const handleMouseLeave = () => {
    setIsHover(false);
  };

  return (
    <button
      onClick={onClick}
      className={mod.button}
      style={{
        background:
          theme === 'black' && isHover
            ? '#575862'
            : theme === 'gray' && isHover
            ? '#e3e0eb'
            : theme === 'white' && isHover
            ? '#f4f3f7'
            : theme === 'black'
            ? '#25262c'
            : theme === 'white'
            ? '#ffffff'
            : theme === 'gray'
            ? '#f4f3f7'
            : '#25262c' && isHover
            ? '#575862'
            : '#25262c',
        paddingLeft: paddingY ? `${paddingY}px` : '5px',
        paddingRight: paddingY ? `${paddingY}px` : '5px',
        paddingTop: paddingX ? `${paddingX}px` : '5px',
        paddingBottom: paddingX ? `${paddingX}px` : '5px',
        justifyContent: flex,
        width: fullWidth ? '100%' : '',
        border: theme === 'white' ? '1px solid #25262c' : '',
        borderRadius: radius,
      }}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}>
      {icon ? (
        <span
          style={{
            marginRight: text ? '8px' : '0',
          }}>
          {icon}
        </span>
      ) : (
        ''
      )}
      {text ? (
        <Typogrphy
          text={text}
          value={'button'}
          color={
            theme === 'white'
              ? '#25262c'
              : theme === 'black'
              ? '#ffffff'
              : theme === 'gray'
              ? '#25262c'
              : '#ffffff'
          }
        />
      ) : (
        ''
      )}
    </button>
  );
};

export default MainButton;
