import React from 'react';
import { Link } from 'react-router-dom';
import { SubButtonProps } from './type';
import Typography from '../Typography/Typography';
import { Icon } from '../../utils/icon/icon';
import mod from './SubButton.module.scss';

const SubButton: React.FC<SubButtonProps> = ({
  icon,
  text,
  color,
  link,
  to,
  iconColor,
  onClick,
}) => {
  const renderIcon = () => {
    if (!icon) return null;
    return (
      <span style={{ marginRight: text ? '8px' : '0' }}>
        <Icon
          name={icon}
          fill={
            iconColor === 'black'
              ? '#25262c'
              : color === 'gray'
              ? '#c3c4C6'
              : color === 'orange'
              ? '#e26d48'
              : '#25262c' || 'black'
          }
        />
      </span>
    );
  };

  const renderText = () => {
    if (!text) return null;
    return (
      <Typography
        text={text}
        value={'button'}
        color={
          color === 'black'
            ? '#25262c'
            : color === 'gray'
            ? '#c3c4C6'
            : color === 'orange'
            ? '#e26d48'
            : '#25262c'
        }
      />
    );
  };
  return (
    <>
      {link ? (
        <Link to={to || ''} className={mod.button}>
          {renderIcon()}
          {renderText()}
        </Link>
      ) : (
        <button className={mod.button} onClick={onClick}>
          {renderIcon()}
          {renderText()}
        </button>
      )}
    </>
  );
};

export default SubButton;
