export type SubButtonProps = {
  icon?: any;
  text?: string;
  color?: 'black' | 'gray' | 'orange';
  to?: string;
  link: boolean;
  iconColor?: 'black' | 'gray' | 'orange';
  onClick?: () => void;
};
