import React from 'react';
import Typogrphy from '../Typography/Typography';
import MainButton from '../MainButton/MainButton';
import TextInput from '../TextInput/TextInput';
import { Icon } from '../../utils/icon/icon';
import { ModalFormProps } from './type';
import mod from './ModalForm.module.scss';

const ModalForm: React.FC<ModalFormProps> = ({ isOpen, setIsOpen }) => {
  if (!isOpen) return null;

  return (
    <div className={mod.overlay}>
      <div className={mod.modal}>
        <button className={mod.button} onClick={() => setIsOpen(false)}>
          <Icon name="closeIcon" />
        </button>
        <Typogrphy
          value="h2"
          text="Change Password"
          fullWidth={true}
          centered={true}
          mb={32}
        />
        <TextInput
          name="oldPassword"
          placeholder="Old password"
          type="password"
        />
        <TextInput
          name="newPassword"
          placeholder="New password"
          type="password"
        />
        <TextInput
          name="repeatPassword"
          placeholder="Repeat the password"
          type="password"
        />
        <MainButton
          text="Confirm"
          theme="black"
          flex="center"
          fullWidth={true}
          radius={10}
          paddingX={16}
        />
      </div>
    </div>
  );
};

export default ModalForm;
