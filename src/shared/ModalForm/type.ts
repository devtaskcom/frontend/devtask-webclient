export interface ModalFormProps {
  isOpen: boolean;
  setIsOpen: (value: boolean) => void;
}
