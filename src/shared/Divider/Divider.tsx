import { FC, HTMLAttributes } from 'react';
import classNames from 'classnames';
import mode from './Divider.module.scss';

interface DividerProps extends HTMLAttributes<HTMLHRElement> {
  className?: string;
}

const Divider: FC<DividerProps> = ({ className, ...props }) => {
  const classes = classNames(mode.divider, className);

  return <hr {...props} className={classes} />;
};

export default Divider;
