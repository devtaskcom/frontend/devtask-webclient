import React from 'react'
import cl from 'classnames'
import { MessageProps } from './types'
import mod from './Message.module.scss'

const Message: React.FC<MessageProps> = ({ type, errors, name, ...props }) => {
  const error = errors?.[name]

  return (
    <span
      {...props}
      className={cl(mod.inputDefault, {
        [mod.errorMessage]: type === 'error',
      })}
    >
      {error?.message}
    </span>
  )
}

export default Message
