
export type MessageProps = {
  type: 'error' | 'success';
  errors?: CustomFieldErrors;
  name: keyof CustomFieldErrors;
  [key: string]: unknown;
};

type CustomFieldErrors = {
  [key: string]: {
    message: string;
  };
};