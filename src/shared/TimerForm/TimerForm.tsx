import React from 'react';
import mod from './TimeForm.module.scss';

type TimeFormProps = {
  hours?: number;
  minutes?: number;
  seconds?: number;
  color?: 'black' | 'green' | 'orange';
};

const TimeForm: React.FC<TimeFormProps> = ({ hours = 0, minutes = 0, seconds = 0, color }) => {
  // const [paused, setPaused] = React.useState(false);
  // const [over, setOver] = React.useState(false);
  const [[h, m, s], setTime] = React.useState([hours, minutes, seconds]);

  const tick = () => {
    // if (paused || over) return;

    if (h === 0 && m === 0 && s === 0) {
      // setOver(true);
    } else if (m === 0 && s === 0) {
      setTime([h - 1, 59, 59]);
    } else if (s == 0) {
      setTime([h, m - 1, 59]);
    } else {
      setTime([h, m, s - 1]);
    }
  };

  // const reset = () => {
  //   setTime([hours, minutes, seconds]);
  //   setPaused(false);
  //   setOver(false);
  // };

  React.useEffect(() => {
    const timerID = setInterval(() => tick(), 1000);
    return () => clearInterval(timerID);
  });

  return (
    <div>
      {hours > 0 ? (
        <span
          style={{
            color:
              color === 'black'
                ? '#25262c'
                : color === 'green'
                ? '#a2e248'
                : color === 'orange'
                ? '#e26d48'
                : '#25262c',
          }}
          className={mod.numCol}>{`${h.toString().padStart(2, '0')}:`}</span>
      ) : (
        ''
      )}
      {minutes >= 0 ? (
        <span
          style={{
            color:
              color === 'black'
                ? '#25262c'
                : color === 'green'
                ? '#a2e248'
                : color === 'orange'
                ? '#e26d48'
                : '#25262c',
          }}
          className={mod.numCol}>{`${m.toString().padStart(2, '0')}:`}</span>
      ) : (
        ''
      )}
      {seconds >= 0 ? (
        <span
          style={{
            color:
              color === 'black'
                ? '#25262c'
                : color === 'green'
                ? '#a2e248'
                : color === 'orange'
                ? '#e26d48'
                : '#25262c',
          }}
          className={mod.numCol}>{`${s.toString().padStart(2, '0')}`}</span>
      ) : (
        ''
      )}
      {/* <div>{over ? "Time's up!" : ''}</div> */}
      {/* <button onClick={() => setPaused(!paused)}>{paused ? 'Resume' : 'Pause'}</button>
      <button onClick={() => reset()}>Restart</button> */}
    </div>
  );
};

export default TimeForm;
