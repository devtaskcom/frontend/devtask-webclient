import { Routes, Route } from 'react-router-dom';
import HomePage from '../pages/HomePage/HomePage';
import AccountPage from '../pages/Account/AccountPage';
import Login from '../pages/Auth/LoginPage/LoginPage';
import Register from '../pages/Auth/RegisterPage/RegisterPage';
import Auth from '../pages/Auth/Auth';
import ContentLayout from '../pages/ContentLayout';

function App() {
  return (
    <div className="container">
      <Routes>
        <Route path="/auth" element={<Auth />}>
          <Route index element={<Login />} />
          <Route path="register" element={<Register />} />
        </Route>
        <Route path="/" element={<ContentLayout />}>
          <Route index element={<HomePage />} />
          <Route path="/account" element={<AccountPage />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
