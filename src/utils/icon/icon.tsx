import { type CSSProperties } from 'react';
import cn from 'classnames';

import { getIcon, type IconName } from './icons';
import styles from './icon.module.scss';

interface IconProps {
  name: IconName;
  className?: string;
  style?: CSSProperties;
  fill?: string;
  onClick?: () => void;
}

export const Icon = ({ name, className, style, fill, onClick }: IconProps) => {
  const SvgIcon = getIcon(name);
  if (onClick) {
    return (
      <button type="button" className={styles.button} onClick={onClick}>
        <SvgIcon style={style} className={cn(styles.icon, className)} />
      </button>
    );
  }
  return (
    <SvgIcon className={cn(styles.icon, className)} style={style} fill={fill} />
  );
};
