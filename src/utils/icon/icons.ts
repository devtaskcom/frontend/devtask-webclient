import { ReactComponent as burgerIcon } from '../../assets/icons/burger-icon.svg';
import { ReactComponent as plusIcon } from '../../assets/icons/plus-icon.svg';
import { ReactComponent as logo } from '../../assets/icons/logo.svg';
import { ReactComponent as openEye } from '../../assets/icons/eye-outline-open.svg';
import { ReactComponent as closeEye } from '../../assets/icons/eye-outline-close.svg';
import { ReactComponent as exitIcon } from '../../assets/icons/exit-icon.svg';
import { ReactComponent as backIcon } from '../../assets/icons/back-icon.svg';
import { ReactComponent as settingsIcon } from '../../assets/icons/settings-icon.svg';
import { ReactComponent as plusCircleIcon } from '../../assets/icons/plus-circle-icon.svg';
import { ReactComponent as closeIcon } from '../../assets/icons/close-icon.svg';
import { ReactComponent as profile } from '../../assets/icons/profile-icon.svg';
import { ReactComponent as logOut } from '../../assets/icons/log-out-outline.svg';
import { ReactComponent as trash } from '../../assets/icons/trash.svg';
import { ReactComponent as folder } from '../../assets/icons/folder.svg';
import { ReactComponent as home } from '../../assets/icons/home.svg';
import { ReactComponent as check } from '../../assets/icons/checkmark-black.svg';

const icons = {
  burgerIcon,
  plusIcon,
  logo,
  openEye,
  closeEye,
  exitIcon,
  backIcon,
  settingsIcon,
  plusCircleIcon,
  closeIcon,
  profile,
  logOut,
  trash,
  folder,
  home,
  check,
};

export type IconName = keyof typeof icons;

export const getIcon = (name: IconName) => icons[name];
