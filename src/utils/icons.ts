import burgerIcon from '../assets/icons/burger-icon.svg';
import plusIcon from '../assets/icons/plus-icon.svg';
import logo from '../assets/icons/logo.svg';
import openEye from '../assets/icons/eye-outline-open.svg';
import closeEye from '../assets/icons/eye-outline-close.svg';
import github from '../assets/icons/github-outline-1.svg';
import google from '../assets/icons/google-outline-1.svg';
import avatar from '../assets/icons/ava.svg';
import profile from '../assets/icons/profile-icon.svg';
import logOut from '../assets/icons/log-out-outline.svg';
import trash from '../assets/icons/trash.svg';
import folder from '../assets/icons/folder.svg';
import home from '../assets/icons/home.svg';
import check from '../assets/icons/checkmark-black.svg';


export const icons = {
  burgerIcon,
  plusIcon,
  logo,
  openEye,
  closeEye,
  github,
  google,
  avatar,
  profile,
  logOut,
  trash,
  folder,
  home,
  check,
};
