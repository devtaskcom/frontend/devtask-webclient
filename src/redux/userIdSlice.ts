import { createSlice, PayloadAction } from '@reduxjs/toolkit';
const { localStorage } = window;

interface userIdSliceTypes {
  userId: string;
}

const initialState: userIdSliceTypes = {
  userId: '',
};

const userIdSlice = createSlice({
  name: 'userId',
  initialState,
  reducers: {
    setUserId(state, action: PayloadAction<string>) {
      state.userId = action.payload;
      localStorage.setItem('user', JSON.stringify(state.userId));
    },
  },
});

export const { setUserId } = userIdSlice.actions;

export default userIdSlice.reducer;
