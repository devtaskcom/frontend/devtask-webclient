import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { baseUrl } from "../lib/baseURL"
import { LoginDataReq, LoginDataRes } from "../../types/authDTO/authDTO"

export const authApi = createApi({
  reducerPath: 'authApi',
  baseQuery: fetchBaseQuery({ baseUrl }),
  tagTypes: ['auth'],
  endpoints: (builder) => ({
    login: builder.mutation<LoginDataRes, LoginDataReq>({
      query: ({ email, password }) =>(
        {
          url: '/auth/sign-in',
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: { email, password },
        }
      ),
      invalidatesTags: ['auth'],
    }),
  }),
})

export const { useLoginMutation } = authApi