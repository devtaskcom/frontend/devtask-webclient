import { Outlet } from 'react-router-dom';
import SideNavigation from '../components/SideNavigation/SideNavigation';
import Header from '../components/Header/Header';
import mod from './ContentLayout.module.scss';

const ContentLayout = () => {
  return (
    <div className={mod.wrapper}>
      <div className={mod.sidebar}>
        <SideNavigation />
      </div>
      <div>
        <div className={mod.header}>
          <Header />
        </div>
        <div className={mod.content}>
          <Outlet />
        </div>
      </div>
    </div>
  );
};

export default ContentLayout;
