import AuthCard from "../../../shared/AuthCard/AuthCard";
import SignInForm from "../../../widgets/signInForm/SignInForm";
import mod from './LoginPage.module.scss';

const Login = () => {
  return (
    <div className={mod.wrapper}>
      <AuthCard>
        <SignInForm />
      </AuthCard>
    </div>
  );
}

export default Login