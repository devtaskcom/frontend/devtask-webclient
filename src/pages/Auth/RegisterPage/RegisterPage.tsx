import { useState } from 'react';
import MainButton from '../../../shared/MainButton/MainButton';
import TextInput from '../../../shared/TextInput/TextInput';
import Typogrphy from '../../../shared/Typography/Typography';
import mod from './RegisterPage.module.scss';

const Register = () => {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [nickName, setNickName] = useState(null);

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    if (name === 'nickName') {
      setNickName(value);
    }
    if (name === 'email') {
      setEmail(value);
    }
    if (name === 'password') {
      setPassword(value);
    }
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();

    let obj = {
      username: nickName,
      email: email,
      password: password,
    };

    fetch('http://apicalc.click/api/v1/auth/sign-up', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(obj),
    }).then((response) => response.json())
      .catch(error => {
      console.error(error);
    });
  };

  return (
    <div className={mod.register}>
      <Typogrphy centered={true} text='Account registration' value='h2' mb={32} />
      <form className={mod.form}>
        <TextInput
          name={'email'}
          type={'email'}
          placeholder={'Email'}
          mb={16}
          onChange={(e) => handleInputChange(e)}
        />
        <TextInput
          name={'password'}
          type={'password'}
          placeholder={'Password'}
          mb={16}
          onChange={(e) => handleInputChange(e)}
        />
        <TextInput
          name={'nickName'}
          type={'text'}
          placeholder={'Nickname'}
          mb={16}
          onChange={(e) => handleInputChange(e)}
        />
        {/* <div className={mod.formBlock}>
          <TextInput name={'firstname'} type={'text'} placeholder={'FirstName'} />
          <TextInput name={'lastname'} type={'text'} placeholder={'LastName'} />
        </div>
        <div className={mod.formBlock}>
          <MainButton
            text={'Sing Up'}
            fullWidth={true}
            flex={'center'}
            paddingX={16}
            radius={10}
            theme={'gray'}
          />
          <MainButton
            text={'Sing Up'}
            fullWidth={true}
            flex={'center'}
            paddingX={16}
            radius={10}
            theme={'gray'}
          />
        </div>
        <TextInput name={'date'} type={'date'} placeholder={'Age'} mb={16} /> */}
        <MainButton
          onClick={(e) => handleSubmit(e)}
          text={'Sing Up'}
          fullWidth={true}
          flex={'center'}
          paddingX={16}
          radius={10}
        />
      </form>
    </div>
  );
};

export default Register;
