import { Outlet } from 'react-router-dom';
import AuthHeader from '../../components/AuthHeader/AuthHeader';
import AuthFooter from '../../components/AuthFooter/AuthFooter';
import mod from './Auth.module.scss';
import ProfileWithTooltip from "../../widgets/profileWithTooltip/ProfileWithTooltip";

const Auth = () => {
  return (
    <>
      <AuthHeader />
      <ProfileWithTooltip />
      <div className={mod.auth}>
        <Outlet />
      </div>
      <AuthFooter />
    </>
  );
};

export default Auth;
