import { useEffect } from "react"
import { useNavigate } from "react-router-dom"

const HomePage = () => {
  const navigate = useNavigate()
  useEffect(() => {
    const user = localStorage.getItem('user');
    if (!user) {
      navigate('/auth')
    }
  }, [navigate])
  
  return (
    <>
      <h1>Здесь будет главная страничка</h1>
    </>
  )
}

export default HomePage