export type LoginDataReq = {
  email: string;
  password: string
}

export type LoginDataRes = {
  id: string;
  email: string;
  username: string;
  display_name: string;
}