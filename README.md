# \_DevTask-webclient

## Getting started

## Run app

npm run dev

## Стек технологий

React
Redux / RTK Query
Typescript
React-hooks-form
CSS Modules + SCSS
Vite
...

../src/

> shared
>
> Изолированные компоненты общего назначени

> widgets
>
> Самостоятельные и полноценные блоки страницы с конкретными действиями;

> services
>
> RTK Query

> utils
>
> глобальные функции и константы: fetch-обёртки, форматировщики текста…

> hooks
>
> Предназначенные для многократного использования нестандартные хуки React: `useInput`, `useAuth`

> types
>
> Глобальные типы TypeScript

> styles
>
> Глобальные стили, CSS-переменные для всего приложения

> assets
>
> Ресурсы (svg-иконки, изображения, шрифты...)

---

> app
>
> Входная точка приложения: компонент App, маршруты, инициализирующая логика (store, например), настройки приложения

## `app`

    ../src/app/
    	App.tsx
    		(export default function App() { … })
    	routes.tsx (или запись напрямую в `App.tsx`)
    	store.ts
    	(…)

## `pages`

../src/pages/
Home.tsx
(export function Home(…) { … })
Profile.tsx
(export function Profile(…) { … })
index.ts
(export { Home } from './Home')
(export { Profile } from './Profile')

## `widgets`

    ../src/widgets/
    	NavbarWidget/
      ui
        NavbarWidget.module.scss
        Navbar.test.tsx
        Navbar.tsx
    	index.ts
    		(export { NavbarWidget } from './NavbarWidget/NavbarWidget')

    // import { NavbarWidget } from '../widgets/index'

## `services`

    ../src/services/
    /api
      authAPI.ts
      todoAPI.ts

    /slices
      userSlice.ts

## `shared`

    ../src/shared
    	Input/
    		Input.tsx
    			(export function Input(…) { … })
    		Input.module.scss
    	Checkbox/
    		Checkbox.tsx
    			(export function Checkbox(…) { … })
    		Checkbox.module.scss
    	index.ts
    		(export { Block } from './Block/Block')
    		(export { Checkbox } from './Checkbox/Checkbox')

## `utils`

    ../src/utils/
    	constants.ts
    	ClassNames.ts
    		(export default function ClassNames(…) { … })

## `styles`

    ../src/styles/
    	global.scss
    	variables.css
    	index.scss (только импорты: `normalize`, шрифты и т. п.)
    		(@import "~normalize.css")
    		(@import "./variables.css")
    		(@import "./global.scss")
    		(…)

## `assets`

    ../src/assets/
    	fonts/
    		Inter-Regular.ttf
    		Inter-SemiBold.ttf
    		Inter.css
    			(@font-face { … })

    // @import "../assets/fonts/Inter.css";

## Именование

При именовании будем использовать файлов и папок с компонентами и сервисами - `PascalCase`.

https://github.com/airbnb/javascript/tree/master/react#naming

// bad
import reservationCard from './ReservationCard';

// good
import ReservationCard from './ReservationCard';

    ../src/shared/PrettyCheckbox/
    	PrettyCheckbox.tsx
    		(export function PrettyCheckbox(…) { … })
    	PrettyCheckbox.module.scss

> Название папки с компонентом, имя файла и функции должны точно совпадать.

Для остального именовать в `camelCase`.
